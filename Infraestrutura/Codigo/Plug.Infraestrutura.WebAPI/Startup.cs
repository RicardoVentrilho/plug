﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Plug.Infraestrutura.WebAPI.Utilitarios;

namespace Plug.Infraestrutura.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            ConfiguracaoWebApi.Instancia.Inicie();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfiguracaoWebApi.Instancia.ConfigureServicos(Configuration, services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ConfiguracaoWebApi.Instancia.ConfigureAmbiente(app, env);
        }
    }
}
