﻿using Microsoft.Extensions.DependencyInjection;
using Plug.Infraestrutura.Interfaces;
using Plug.Infraestrutura.Interfaces.Repositorio;
using Plug.Infraestrutura.Objetos.UsuarioObj;
using Plug.Infraestrutura.Repositorio;

namespace Plug.Infraestrutura.WebAPI.Configuracoes
{
    public class InjDepInfraestruturaConfig : IInjecaoDeDependenciaConfig
    {
        public void ConfigureInjecoesDeDependencias(IServiceCollection services)
        {
            services.AddScoped<IRepositorioUsuario, RepositorioUsuario>();
            services.AddTransient<ValidacoesUsuario, ValidacoesUsuario>();
        }
    }
}
