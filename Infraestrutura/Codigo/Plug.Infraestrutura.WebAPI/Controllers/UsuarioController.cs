﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Plug.Infraestrutura.DTOs;
using Plug.Infraestrutura.Enumeradores.Constantes;
using Plug.Infraestrutura.Interfaces.Repositorio;
using Plug.Infraestrutura.Objetos.UsuarioObj;
using Plug.Infraestrutura.WebAPI.Utilitarios;

namespace Plug.Infraestrutura.WebAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route(APIConstantes.ROTA_PADRAO)]
    [Produces(APIConstantes.APP_JSON)]
    public class UsuarioController : ControladorPadrao<Usuario, DtoUsuario, DtoUsuarioParcial, ValidacoesUsuario, IRepositorioUsuario>
    {
        public ProvedorDeAutenticacao ProvedorDeAutenticacao { get; set; }

        public UsuarioController(ValidacoesUsuario validacao, IRepositorioUsuario repositorio) : base(validacao, repositorio)
        {
            ProvedorDeAutenticacao = new ProvedorDeAutenticacao(repositorio);
        }

        [AllowAnonymous]
        public override ActionResult Cadastre([FromBody] DtoUsuario dto)
        {
            return base.Cadastre(dto);
        }

        [AllowAnonymous]
        [HttpPost(APIConstantes.AUTENTIQUE)]
        public ActionResult Autentique(string login, string senha)
        {
            var usuario = ProvedorDeAutenticacao.Autentique(login, senha);

            if (usuario == null)
            {
                return BadRequest(new { message = "Usuário ou senha estão incorretos." });
            }

            return Ok(usuario);
        }
    }
}
