﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Plug.Infraestrutura.DTOs;
using Plug.Infraestrutura.Enumeradores.Constantes;
using Plug.Infraestrutura.ObjetoAbstratoObj;
using Plug.Infraestrutura.Repositorio;
using Plug.Infraestrutura.Validacoes;
using System;

namespace Plug.Infraestrutura.WebAPI.Controllers
{
    [ApiController]
    [Route(APIConstantes.ROTA_PADRAO)]
    [Produces(APIConstantes.APP_JSON)]
    public class ControladorPadrao<T, TDto, TDtoParcial, TValidacao, TRepositorio> : ControllerBase
        where T : ObjetoAbstratoComId, new()
        where TValidacao : ValidacoesAbstrata<T>
        where TRepositorio : IRepositorioGenerico<T>
    {
        public TValidacao Validacao { get; private set; }

        public TRepositorio Repositorio { get; private set; }

        public ControladorPadrao(TValidacao validacao, TRepositorio repositorio)
        {
            Validacao = validacao;

            Repositorio = repositorio;
        }

        [HttpGet("paginado")]
        public virtual ActionResult<DtoPaginacao<TDto>> ConsulteListaPorDemanda(int? paginaAtual, int? totalPorPagina)
        {
            var paginacao = Repositorio.ConsulteListaPorDemanda(paginaAtual, totalPorPagina);

            var dtoPaginacao = Mapper.Map<DtoPaginacao<TDto>>(paginacao);

            return Ok(dtoPaginacao);
        }

        [HttpGet]
        public virtual ActionResult<TDto> Consulte(Guid id)
        {
            var objeto = Repositorio.Consulte(id);

            var dto = Mapper.Map<TDto>(objeto);

            return Ok(dto);
        }

        [HttpPost]
        public virtual ActionResult Cadastre([FromBody] TDto dto)
        {
            var objeto = Mapper.Map<T>(dto);

            Validacao.AssineRegrasCadastroImpeditivo();

            var resultado = Validacao.Valide(objeto);

            resultado.AssegureSucesso();

            Repositorio.Cadastre(objeto);

            Mapper.Map(objeto, dto);

            return Ok(dto);
        }

        [HttpPost(APIConstantes.VALIDE)]
        public virtual ActionResult<AgrupadorDeValidacao> ValideCadastro([FromBody] TDto dto)
        {
            var objeto = Mapper.Map<T>(dto);

            Validacao.AssineRegrasCadastroImpeditivo();
            Validacao.AssineRegrasCadastroNaoImpeditivo();

            var resultado = Validacao.Valide(objeto);

            if (resultado.NaoEhValido())
            {
                return BadRequest(resultado);
            }

            return Ok(resultado);
        }

        [HttpPut]
        public virtual ActionResult Atualize([FromBody] TDto dto)
        {
            var objeto = Mapper.Map<T>(dto);

            Validacao.AssineRegrasAtualizacaoImpeditiva();
            Validacao.AssineRegrasAtualizacaoNaoImpeditiva();

            var resultado = Validacao.Valide(objeto);

            if (resultado.NaoEhValido())
            {
                return BadRequest(resultado);
            }

            Repositorio.Atualize(objeto);

            Mapper.Map(objeto, dto);

            return Ok(dto);
        }

        [HttpPut(APIConstantes.VALIDE)]
        public virtual ActionResult<AgrupadorDeValidacao> ValideAtualizacao([FromBody] TDto dto)
        {
            var objeto = Mapper.Map<T>(dto);

            Validacao.AssineRegrasAtualizacaoImpeditiva();
            Validacao.AssineRegrasAtualizacaoNaoImpeditiva();

            var resultado = Validacao.Valide(objeto);

            if (resultado.NaoEhValido())
            {
                return BadRequest(resultado);
            }

            return Ok(resultado);
        }

        [HttpDelete]
        public virtual ActionResult Exclua(Guid id)
        {
            Validacao.AssineRegrasExclusaoImpeditiva();

            var objeto = Repositorio.Consulte(id);

            var resultado = Validacao.Valide(objeto);

            if (resultado.NaoEhValido())
            {
                return BadRequest(resultado);
            }

            Repositorio.Exclua(id);

            return Ok();
        }

        [HttpDelete(APIConstantes.VALIDE)]
        public virtual ActionResult<AgrupadorDeValidacao> ValideExclusao(Guid id)
        {
            Validacao.AssineRegrasExclusaoImpeditiva();
            Validacao.AssineRegrasExclusaoNaoImpeditiva();

            var objeto = Repositorio.Consulte(id);

            var resultado = Validacao.Valide(objeto);

            if (resultado.NaoEhValido())
            {
                return BadRequest(resultado);
            }

            return Ok(resultado);
        }
    }
}
