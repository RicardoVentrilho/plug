﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Plug.Infraestrutura.Enumeradores.Constantes;
using Plug.Infraestrutura.Validacoes;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace Plug.Infraestrutura.WebAPI.Utilitarios
{
    public class MiddlewareWebApi
    {
        private readonly RequestDelegate _proximo;

        public MiddlewareWebApi(RequestDelegate proximo)
        {
            _proximo = proximo;
        }

        public async Task Invoke(HttpContext contexto)
        {
            try
            {
                await _proximo(contexto);
            }
            catch (Exception excecao)
            {
                await TrateErroAsync(contexto, excecao);
            }
        }

        private static Task TrateErroAsync(HttpContext contexto, Exception excecao)
        {
            var codigo = HttpStatusCode.InternalServerError;
            var resultado = string.Empty;

            if (excecao is InconsistenciaExcecao)
            {
                codigo = HttpStatusCode.BadRequest;
                resultado = LowercaseJsonSerializer.SerializeObject(((InconsistenciaExcecao)excecao).Validacoes);
            }
            else
            {
                GerenciadorDeErros.Publique(excecao);

                resultado = LowercaseJsonSerializer.SerializeObject(new { error = excecao.Message });
            }

            contexto.Response.ContentType = APIConstantes.APP_JSON;
            contexto.Response.StatusCode = (int)codigo;

            return contexto.Response.WriteAsync(resultado);
        }

        public class LowercaseJsonSerializer
        {
            private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                ContractResolver = new LowercaseContractResolver()
            };

            public static string SerializeObject(object o)
            {
                return JsonConvert.SerializeObject(o, Formatting.None, Settings);
            }

            public class LowercaseContractResolver : DefaultContractResolver
            {
                protected override string ResolvePropertyName(string propertyName)
                {
                    return new string(propertyName.Select((ch, index) => (index == 0) ? char.ToLower(ch) : ch).ToArray()); ;
                }
            }
        }
    }
}
