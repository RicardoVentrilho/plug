﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Plug.Infraestrutura.Enumeradores.Constantes;
using Plug.Infraestrutura.Interfaces;
using Plug.Infraestrutura.WebAPI.Configuracoes;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Plug.Infraestrutura.WebAPI.Utilitarios
{
    public class ConfiguracaoWebApi
    {
        private static ConfiguracaoWebApi _instancia;
        private const string PLUG = "Plug";
        private const string NS_WEB_API = "WebAPI";
        private const string NS_SERVICO_MAPEADOR = "ServicoMapeador";

        public static ConfiguracaoWebApi Instancia => _instancia ?? (_instancia = new ConfiguracaoWebApi());

        public void ConfigureServicos(IConfiguration configuracao, IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddApiVersioning(o => o.ApiVersionReader = new HeaderApiVersionReader(APIConstantes.API_VERSION));

            var sessaoConfiguracaoToken = configuracao.GetSection("ConfiguracaoToken");
            services.Configure<ConfiguracaoToken>(sessaoConfiguracaoToken);

            var configuracaoToken = sessaoConfiguracaoToken.Get<ConfiguracaoToken>();
            var key = Encoding.ASCII.GetBytes(configuracaoToken.Segredo);

            ProvedorDeAutenticacao.ConfigureToken(configuracaoToken);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Plug API",
                    Description = "As APIs são uma forma de comunicação que utiliza o protocolo http.",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "José Ricardo Ventrilho",
                        Email = "ricardoventrilho@gmail.com"
                    },
                    License = new License
                    {
                        Name = "Plug Corporation @ 2019"
                    }
                });

                c.DocumentFilter<SchemeFilter>();
                c.OperationFilter<AddVersionHeaderFilter>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            ConfigureInjecoesDeDependencias(services);
        }

        public void Inicie()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.RecognizePostfixes("Parcial");
                cfg.RecognizePrefixes("Dto");
                cfg.AllowNullCollections = true;
                cfg.AllowNullDestinationValues = true;

                ConfigureMapeadores(cfg);
            });
        }

        private void ConfigureMapeadores(IMapperConfigurationExpression configuracao)
        {
            var assembliesServicoMapeador = new List<Assembly>
            {
                Assembly.Load("Plug.Infraestrutura")
            };

            try
            {
                // TODO: Adicionar esse comportamento em uma extension method.
                var nucleo = Assembly.Load("Plug.Nucleo.ServicoMapeador");
                assembliesServicoMapeador.Add(nucleo);
            }
            catch (Exception)
            {
            }

            foreach (var assemblyServicoMapeador in assembliesServicoMapeador)
            {
                var tiposMapperConfig = assemblyServicoMapeador.GetExportedTypes().Where(tipo => tipo.GetInterfaces().Contains(typeof(IAutoMapperConfig)));

                foreach (var tipoMapperConfig in tiposMapperConfig)
                {
                    var autoMapperConfig = (IAutoMapperConfig)Activator.CreateInstance(tipoMapperConfig);

                    autoMapperConfig.ConfigureMapeamento(configuracao);
                }
            }
        }

        public void ConfigureAmbiente(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Autenticação
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();

            // Documentação
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Plug API Version 1");
                c.RoutePrefix = string.Empty;
            });

            // Ambiente de desenvolvimento
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Configuração do middleware
            app.UseMiddleware(typeof(MiddlewareWebApi));
            app.UseMvc();
        }

        private void ConfigureInjecoesDeDependencias(IServiceCollection services)
        {
            var assembliesWeb = AppDomain.CurrentDomain.GetAssemblies().Where(EhAssemblyWeb);

            foreach (var assemblyWeb in assembliesWeb)
            {
                var tiposIDConfig = assemblyWeb.GetExportedTypes().Where(tipo => tipo.GetInterfaces().Contains(typeof(IInjecaoDeDependenciaConfig)));

                foreach (var tipoIDConfig in tiposIDConfig)
                {
                    var iDConfig = (IInjecaoDeDependenciaConfig)Activator.CreateInstance(tipoIDConfig);

                    iDConfig.ConfigureInjecoesDeDependencias(services);
                }
            }
        }

        private bool EhAssemblyWeb(Assembly assembly)
        {
            return assembly.FullName.StartsWith(PLUG) && assembly.FullName.Contains(NS_WEB_API);
        }

        private bool EhAssemblyServicoMapeador(Assembly assembly)
        {
            return assembly.FullName.StartsWith(PLUG) && assembly.FullName.Contains(NS_SERVICO_MAPEADOR);
        }

        public class SchemeFilter : IDocumentFilter
        {
            public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
            {
                swaggerDoc.Schemes = new string[] { "http", "https" };
            }
        }

        public class AddVersionHeaderFilter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                if (operation.Parameters == null)
                    operation.Parameters = new List<IParameter>();

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "api-version",
                    In = "header",
                    Type = "string",
                    Required = false
                });
            }
        }
    }
}
