﻿using Microsoft.IdentityModel.Tokens;
using Plug.Infraestrutura.Interfaces.Repositorio;
using Plug.Infraestrutura.WebAPI.Configuracoes;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Plug.Infraestrutura.WebAPI.Utilitarios
{
    public class ProvedorDeAutenticacao
    {
        private readonly IRepositorioUsuario _repositorioUsuario;
        private static ConfiguracaoToken _configuracaoToken;

        public ProvedorDeAutenticacao(IRepositorioUsuario repositorio)
        {
            _repositorioUsuario = repositorio;
        }

        public string Autentique(string login, string senha)
        {
            var usuario = _repositorioUsuario.Consulte(login, senha);

            if (usuario == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_configuracaoToken.Segredo);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuario.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public static void ConfigureToken(ConfiguracaoToken configuracao)
        {
            _configuracaoToken = configuracao;
        }
    }
}
