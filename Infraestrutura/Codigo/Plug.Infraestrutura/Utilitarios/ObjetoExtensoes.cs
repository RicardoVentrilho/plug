﻿using Plug.Infraestrutura.Enumeradores.Constantes;
using System;
using System.Globalization;
using System.Linq;

namespace Plug.Infraestrutura.Utilitarios
{
    public static class ObjetoExtensoes
    {
        public static bool PossuiValor(this object objeto)
        {
            return objeto != null;
        }

        public static bool PossuiValor(this string objeto)
        {
            return objeto != null && !string.IsNullOrEmpty(objeto);
        }

        public static bool PossuiValor<T>(this T? objetoNullable)
            where T : struct
        {
            return objetoNullable.HasValue && objetoNullable.Value.PossuiValor();
        }

        public static bool PossuiSomenteDigitos(this string objeto)
        {
            if (!objeto.PossuiValor())
            {
                return true;
            }

            if (objeto.Where(x => char.IsNumber(x)).Count() == objeto.Count())
            {
                return true;
            }

            return false;
        }

        public static IQueryable<T> Pagine<T>(this IQueryable<T> query, ref int? paginaAtual, ref int? quantidadePorPagina)
        {
            if (!paginaAtual.PossuiValor())
            {
                paginaAtual = APIConstantes.PAGINA_ATUAL_PADRAO;
            }

            if (paginaAtual.GetValueOrDefault() < APIConstantes.PAGINA_ATUAL_PADRAO)
            {
                paginaAtual = APIConstantes.PAGINA_ATUAL_PADRAO;
            }

            if (!quantidadePorPagina.PossuiValor())
            {
                quantidadePorPagina = APIConstantes.QUANTIDADE_POR_PAGINA_PADRAO;
            }

            if (quantidadePorPagina.GetValueOrDefault() < 0)
            {
                quantidadePorPagina = 0;
            }

            return query
                .Skip((paginaAtual.GetValueOrDefault() - 1) * quantidadePorPagina.GetValueOrDefault())
                .Take(quantidadePorPagina.GetValueOrDefault());
        }

        public static TimeSpan ToTimeSpan_HHmm(this string tempo)
        {
            return DateTime.ParseExact(tempo, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
        }
    }
}
