﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using Plug.Infraestrutura.Repositorio;
using System;
using System.Data;
using System.Linq;

namespace Plug.Infraestrutura.Utilitarios
{
    public abstract class NHUtils
    {
        private const string PLUG = "Plug";
        private ISession _sessao;

        public NHUtils()
        {
            Configuracao = new Configuration();

            var mapper = new ModelMapper();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.Contains(PLUG));

            foreach (var assembly in assemblies)
            {
                mapper.AddMappings(assembly.GetExportedTypes());
            }

            var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();

            Configuracao.AddMapping(mapping);

            Configuracao.DataBaseIntegration(db =>
            {
                db.ConnectionString = ProvedorDeConexao.ObtenhaConexao();
                db.Dialect<MsSql2012Dialect>();
                db.Driver<SqlClientDriver>();

                db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                db.IsolationLevel = IsolationLevel.ReadUncommitted;

                db.Timeout = 10;

                db.LogFormattedSql = true;
                db.LogSqlInConsole = true;
                db.AutoCommentSql = true;
            });

            Configuracao.SessionFactory().GenerateStatistics();

            FabricaDeSessao = Configuracao.BuildSessionFactory();
        }

        ~NHUtils()
        {
            Sessao.Close();
        }

        protected void ExecuteTransacao(Action acao)
        {
            using (var transacao = Sessao.BeginTransaction())
            {
                try
                {
                    acao();

                    transacao.Commit();
                }
                catch (Exception erro)
                {
                    transacao.Rollback();

                    throw erro;
                }
            }
        }

        protected void ExecuteTransacao(Action<ITransaction> acao)
        {
            using (var transacao = Sessao.BeginTransaction())
            {
                try
                {
                    acao(transacao);

                    transacao.Commit();
                }
                catch (Exception erro)
                {
                    transacao.Rollback();

                    throw erro;
                }
            }
        }


        protected Configuration Configuracao { get; private set; }

        protected ISessionFactory FabricaDeSessao { get; private set; }

        protected ISession Sessao
        {
            get
            {
                if (!_sessao.PossuiValor())
                {
                    _sessao = FabricaDeSessao.OpenSession();
                    _sessao.FlushMode = FlushMode.Commit;
                }

                return _sessao;
            }
        }
    }
}
