﻿namespace Plug.Infraestrutura.DTOs
{
    public class DtoUsuarioParcial
    {
        public string Login { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }
    }
}
