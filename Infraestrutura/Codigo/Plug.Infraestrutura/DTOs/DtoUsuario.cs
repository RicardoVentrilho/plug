﻿namespace Plug.Infraestrutura.DTOs
{
    public class DtoUsuario
    {
        public string Login { get; set; }

        public string Senha { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }
    }
}
