﻿using AutoMapper;
using Plug.Infraestrutura.DTOs;
using Plug.Infraestrutura.Interfaces;
using Plug.Infraestrutura.Objetos.UsuarioObj;

namespace Plug.Infraestrutura.Mapeamentos.AutoMapper
{
    public class UsuarioConfig : IAutoMapperConfig
    {
        public void ConfigureMapeamento(IMapperConfigurationExpression configuracao)
        {
            configuracao
                .CreateMap<Usuario, DtoUsuario>()
                .ReverseMap();

            configuracao
                .CreateMap<Usuario, DtoUsuarioParcial>()
                .ReverseMap();
        }
    }
}
