﻿using Plug.Infraestrutura.Objetos.UsuarioObj;

namespace Plug.Infraestrutura.Mapeamentos.Repositorios
{
    public class UsuarioMap : ObjetoAbstratoComIdMap<Usuario>
    {
        public UsuarioMap()
        {
            Table("SAAA_USUARIO");

            Property(x => x.Login, map => map.Column("USRLOGIN"));

            Property(x => x.Nome, map => map.Column("USRNOME"));

            Property(x => x.Senha, map => map.Column("USRSENHA"));

            Property(x => x.Email, map => map.Column("USREMAIL"));
        }
    }
}
