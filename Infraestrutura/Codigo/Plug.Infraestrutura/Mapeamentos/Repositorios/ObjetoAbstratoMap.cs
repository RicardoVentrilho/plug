﻿using NHibernate.Mapping.ByCode.Conformist;
using Plug.Infraestrutura.ObjetoAbstratoObj;

namespace Plug.Infraestrutura.Mapeamentos.Repositorios
{
    public abstract class ObjetoAbstratoMap<T> : ClassMapping<T>
        where T : ObjetoAbstrato
    {
    }
}
