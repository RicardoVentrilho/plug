﻿using NHibernate.Mapping.ByCode;
using Plug.Infraestrutura.ObjetoAbstratoObj;

namespace Plug.Infraestrutura.Mapeamentos.Repositorios
{
    public abstract class ObjetoAbstratoComIdMap<T> : ObjetoAbstratoMap<T>
        where T : ObjetoAbstratoComId
    {
        public ObjetoAbstratoComIdMap()
        {
            Lazy(false);

            Id(x => x.Id, map =>
            {
                map.Generator(Generators.GuidComb);
                map.Column("ID");
            });
        }
    }
}
