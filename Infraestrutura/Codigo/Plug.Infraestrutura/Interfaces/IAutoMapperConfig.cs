﻿using AutoMapper;

namespace Plug.Infraestrutura.Interfaces
{
    public interface IAutoMapperConfig
    {
        void ConfigureMapeamento(IMapperConfigurationExpression configuracao);
    }
}
