﻿using Microsoft.Extensions.DependencyInjection;

namespace Plug.Infraestrutura.Interfaces
{
    public interface IInjecaoDeDependenciaConfig
    {
        void ConfigureInjecoesDeDependencias(IServiceCollection services);
    }
}
