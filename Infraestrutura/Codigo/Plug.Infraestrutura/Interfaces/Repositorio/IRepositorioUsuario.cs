﻿using Plug.Infraestrutura.Objetos.UsuarioObj;
using Plug.Infraestrutura.Repositorio;

namespace Plug.Infraestrutura.Interfaces.Repositorio
{
    public interface IRepositorioUsuario : IRepositorioGenerico<Usuario>
    {
        Usuario Consulte(string login, string senha);
    }
}
