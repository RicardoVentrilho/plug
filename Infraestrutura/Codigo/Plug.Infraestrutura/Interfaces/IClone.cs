﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Plug.Infraestrutura.Interfaces
{
    public interface IClone<T>
    {
        T Clone();
    }
}
