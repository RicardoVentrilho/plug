﻿using Plug.Infraestrutura.Interfaces.Repositorio;
using Plug.Infraestrutura.Objetos.UsuarioObj;
using System.Linq;

namespace Plug.Infraestrutura.Repositorio
{
    public class RepositorioUsuario : RepositorioGenerico<Usuario>, IRepositorioUsuario
    {
        public Usuario Consulte(string login, string senha)
        {
            return Persistencia.FirstOrDefault(x => x.Login == login && x.Senha == senha);
        }
    }
}
