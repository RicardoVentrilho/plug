﻿using Plug.Infraestrutura.ObjetoAbstratoObj;
using Plug.Infraestrutura.Objetos;
using Plug.Infraestrutura.Utilitarios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Plug.Infraestrutura.Repositorio
{
    public abstract class RepositorioGenerico<T> : NHUtils, IRepositorioGenerico<T>
        where T : ObjetoAbstratoComId
    {
        public IQueryable<T> Persistencia
        {
            get
            {
                return Sessao.Query<T>();
            }
        }

        public virtual List<T> ConsulteLista()
        {
            return Persistencia.ToList();
        }

        public virtual void Cadastre(T objeto)
        {
            ExecuteTransacao(() =>
            {
                Sessao.SaveAsync(objeto);
            });
        }

        public virtual void Atualize(T objeto)
        {
            ExecuteTransacao(() =>
            {
                Sessao.UpdateAsync(objeto);
            });
        }

        public virtual List<T> ConsulteLista(Func<T, bool> filtro)
        {
            return Persistencia.Where(filtro).ToList();
        }

        public virtual T Consulte(Guid id)
        {
            return Persistencia.FirstOrDefault(x => x.Id == id);
        }

        public virtual Paginacao<T> ConsulteListaPorDemanda(int? paginaAtual, int? quantidadePorPagina)
        {
            var registros = Persistencia.Pagine(ref paginaAtual, ref quantidadePorPagina).ToList();
            var totalDeRegistros = Persistencia.Count();
            var totalPorPaginas = quantidadePorPagina.GetValueOrDefault();
            var totalDePaginas = (int)Math.Ceiling((double)totalDeRegistros / (double)totalPorPaginas);

            return new Paginacao<T>
            {
                PaginaAtual = paginaAtual.GetValueOrDefault(),
                Registros = registros,
                TotalDeRegistros = totalDeRegistros,
                TotalPorPagina = totalPorPaginas,
                TotalDePaginas = totalDePaginas
            };
        }

        public virtual void Exclua(Guid id)
        {
            ExecuteTransacao(() =>
            {
                var objeto = Persistencia.FirstOrDefault(x => x.Id == id);

                Sessao.DeleteAsync(objeto);
            });
        }

        public bool Existe(Guid id)
        {
            return Persistencia.Any(x => x.Id == id);
        }

        protected virtual void CadastreOuAtualize(T objeto)
        {
            ExecuteTransacao(() =>
            {
                Sessao.SaveOrUpdateAsync(objeto);
            });
        }

        protected IQueryable<TConceito> CriePersistencia<TConceito>()
            where TConceito : ObjetoAbstrato
        {
            return Sessao.Query<TConceito>();
        }
    }
}
