﻿using Plug.Infraestrutura.ObjetoAbstratoObj;
using Plug.Infraestrutura.Objetos;
using System;
using System.Collections.Generic;

namespace Plug.Infraestrutura.Repositorio
{
    public interface IRepositorioGenerico<T>
        where T : ObjetoAbstratoComId
    {
        List<T> ConsulteLista();

        List<T> ConsulteLista(Func<T, bool> filtro);

        T Consulte(Guid id);

        Paginacao<T> ConsulteListaPorDemanda(int? paginaAtual, int? quantidadePorPagina);

        void Cadastre(T objeto);

        void Atualize(T objeto);

        void Exclua(Guid id);

        bool Existe(Guid id);
    }
}
