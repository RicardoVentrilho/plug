﻿namespace Plug.Infraestrutura.Validacoes
{
    public class InconsistenciaDeValidacao
    {
        public string Mensagem { get; set; }

        public string PropriedadeValidada { get; set; }
    }
}
