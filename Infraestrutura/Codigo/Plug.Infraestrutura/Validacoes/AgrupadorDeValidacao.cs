﻿using System.Collections.Generic;
using System.Linq;

namespace Plug.Infraestrutura.Validacoes
{
    public class AgrupadorDeValidacao
    {
        public AgrupadorDeValidacao()
        {
            Avisos = new List<InconsistenciaDeValidacao>();
            Inconsistencias = new List<InconsistenciaDeValidacao>();
        }

        public IEnumerable<InconsistenciaDeValidacao> Avisos { get; set; }

        public IEnumerable<InconsistenciaDeValidacao> Inconsistencias { get; set; }

        public bool EhValido(bool considereAvisos = false)
        {
            return !Inconsistencias.Any() && (!considereAvisos || !Avisos.Any());
        }

        public bool NaoEhValido(bool considereAvisos = false)
        {
            return !EhValido(considereAvisos);
        }

        public void AssegureSucesso(bool considereAvisos = false)
        {
            if (NaoEhValido(considereAvisos))
            {
                throw new InconsistenciaExcecao(this);
            }
        }
    }
}
