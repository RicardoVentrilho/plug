﻿using FluentValidation;
using Plug.Infraestrutura.Utilitarios;
using System.Linq;

namespace Plug.Infraestrutura.Validacoes
{
    public abstract class ValidacoesAbstrata<T> : AbstractValidator<T>
        where T : new()
    {
        public abstract void AssineRegrasCadastroImpeditivo();

        public abstract void AssineRegrasCadastroNaoImpeditivo();

        public abstract void AssineRegrasAtualizacaoImpeditiva();

        public abstract void AssineRegrasAtualizacaoNaoImpeditiva();

        public abstract void AssineRegrasExclusaoImpeditiva();

        public abstract void AssineRegrasExclusaoNaoImpeditiva();

        public AgrupadorDeValidacao Valide(T objeto)
        {
            if(!objeto.PossuiValor())
            {
                objeto = new T();
            }

            var result = Validate(objeto);

            if (!result.IsValid)
            {
                return new AgrupadorDeValidacao
                {
                    Avisos = result.Errors.Where(x => x.Severity == Severity.Warning).Select(x => new InconsistenciaDeValidacao
                    {
                        Mensagem = x.ErrorMessage,
                        PropriedadeValidada = x.PropertyName
                    }),
                    Inconsistencias = result.Errors.Where(x => x.Severity == Severity.Error).Select(x => new InconsistenciaDeValidacao
                    {
                        Mensagem = x.ErrorMessage,
                        PropriedadeValidada = x.PropertyName
                    })
                };
            }

            return new AgrupadorDeValidacao();
        }
    }
}
