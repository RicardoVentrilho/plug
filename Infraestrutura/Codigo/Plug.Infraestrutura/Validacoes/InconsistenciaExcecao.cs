﻿using System;

namespace Plug.Infraestrutura.Validacoes
{
    public class InconsistenciaExcecao : Exception
    {
        public InconsistenciaExcecao(AgrupadorDeValidacao validacoes)
        {
            Validacoes = validacoes;
        }

        public AgrupadorDeValidacao Validacoes { get; }
    }
}
