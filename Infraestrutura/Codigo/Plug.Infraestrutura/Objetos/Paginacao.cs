﻿using System.Collections.Generic;

namespace Plug.Infraestrutura.Objetos
{
    public class Paginacao<T>
    {
        public int PaginaAtual { get; set; }

        public int TotalDeRegistros { get; set; }

        public int TotalPorPagina { get; set; }

        public int TotalDePaginas { get; set; }

        public List<T> Registros { get; set; }
    }
}
