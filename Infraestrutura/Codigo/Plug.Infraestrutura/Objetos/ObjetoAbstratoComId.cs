﻿using System;

namespace Plug.Infraestrutura.ObjetoAbstratoObj
{
    public abstract class ObjetoAbstratoComId : ObjetoAbstrato
    {
        public virtual Guid Id { get; set; }
    }
}
