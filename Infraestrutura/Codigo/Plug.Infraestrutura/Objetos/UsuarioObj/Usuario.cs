﻿using Plug.Infraestrutura.ObjetoAbstratoObj;

namespace Plug.Infraestrutura.Objetos.UsuarioObj
{
    public class Usuario : ObjetoAbstratoComId
    {
        public virtual string Login { get; set; }

        public virtual string Senha { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Email { get; set; }
    }
}
