﻿using Plug.Infraestrutura.Validacoes;

namespace Plug.Infraestrutura.Objetos.UsuarioObj
{
    public class ValidacoesUsuario : ValidacoesAbstrata<Usuario>
    {
        public override void AssineRegrasAtualizacaoImpeditiva()
        {
        }

        public override void AssineRegrasAtualizacaoNaoImpeditiva()
        {
        }

        public override void AssineRegrasCadastroImpeditivo()
        {
        }

        public override void AssineRegrasCadastroNaoImpeditivo()
        {
        }

        public override void AssineRegrasExclusaoImpeditiva()
        {
        }

        public override void AssineRegrasExclusaoNaoImpeditiva()
        {
        }
    }
}
