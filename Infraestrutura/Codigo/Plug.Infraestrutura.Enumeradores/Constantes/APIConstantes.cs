﻿namespace Plug.Infraestrutura.Enumeradores.Constantes
{
    public class APIConstantes
    {
        public const string ROTA_PADRAO = "api/[controller]";

        public const string API_VERSION = "api-version";

        public const string V1 = "1.0";

        public const string APP_JSON = "application/json";

        public const string VALIDE = "valide";

        public const string AUTENTIQUE = "autentique";

        public const int QUANTIDADE_POR_PAGINA_PADRAO = 50;

        public const int PAGINA_ATUAL_PADRAO = 1;
    }
}
