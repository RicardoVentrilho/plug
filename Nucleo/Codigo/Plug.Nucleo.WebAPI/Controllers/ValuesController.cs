﻿using Microsoft.AspNetCore.Mvc;
using Plug.Infraestrutura.Enumeradores.Constantes;
using System.Collections.Generic;

namespace Plug.Nucleo.WebAPI.Controllers
{
    [ApiController]
    [Route(APIConstantes.ROTA_PADRAO)]
    [ApiVersion(APIConstantes.V1)]
    [Produces(APIConstantes.APP_JSON)]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "Ventrilho", "Dalvo" };
        }

        [HttpGet("id")]
        public ActionResult<string> Get(int id)
        {
            return "Loucura";
        }

        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
