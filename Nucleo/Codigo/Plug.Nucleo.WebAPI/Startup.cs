﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Plug.Infraestrutura.WebAPI.Utilitarios;

namespace Plug.Nucleo.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            ConfiguracaoWebApi.Inicie();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfiguracaoWebApi.ConfigureServicos(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ConfiguracaoWebApi.ConfigureAmbiente(app, env);
        }
    }
}
